import { advanceBlock } from 'openzeppelin-solidity/test/helpers/advanceToBlock';
import { assertRevert } from 'openzeppelin-solidity/test/helpers/assertRevert';
import { ether } from 'openzeppelin-solidity/test/helpers/ether';
import { EVMRevert } from 'openzeppelin-solidity/test/helpers/EVMRevert';
import { increaseTimeTo, duration } from 'openzeppelin-solidity/test/helpers/increaseTime';
import { latestTime } from 'openzeppelin-solidity/test/helpers/latestTime';

const BigNumber = web3.BigNumber;

const should = require('chai')
  .use(require('chai-as-promised'))
  .use(require('chai-bignumber')(BigNumber))
  .should();

BigNumber.config({ DECIMAL_PLACES: 0, ROUNDING_MODE: 0 });

const LandRegistry = artifacts.require('LandRegistry');
const LandRegistryProxy = artifacts.require('LandRegistryProxy');
const MedianizerProxy = artifacts.require('MedianizerProxy');
const Payments = artifacts.require('Payments');
const TokenizedProperty = artifacts.require('TokenizedProperty');
const TokenSale = artifacts.require('TokenSale');
const Whitelist = artifacts.require('Whitelist');
const WhitelistProxy = artifacts.require('WhitelistProxy');

const Medianizer = artifacts.require('Medianizer');

contract('TokenSale', ([from, wallet, investor, cappedInvestor, unauthorized]) => {
  const eGrid = 'CH327417066524';
  const grundstuckNumber = 'CH-ZG-566';

  const conversionRate = 0x000000000000000000000000000000000000000000000020f39d027901310000;
  const cappedLimit = (new BigNumber(50000)).mul(1e18).mul(1e18).div(conversionRate);
  const minInvestment = ether(1);

  const rate = 10;
  const goal = ether(100);
  const cap = ether(1000);
  const authorizedRole = 'authorized';
  const uncappedRole = 'uncapped';

  before(async function () {
    this.landRegistryProxy = await LandRegistryProxy.new({ from });
    this.landRegistryProxy.address.should.be.equal('0x0f5ea0a652e851678ebf77b69484bfcd31f9459b');

    this.medianizerProxy = await MedianizerProxy.new({ from });
    this.medianizerProxy.address.should.be.equal('0xec8be1a5630364292e56d01129e8ee8a9578d7d8');

    this.whitelistProxy = await WhitelistProxy.new({ from });
    this.whitelistProxy.address.should.be.equal('0x85a84691547b7ccf19d7c31977a7f8c0af1fb25a');

    this.landRegistry = await LandRegistry.new({ from });
    await this.landRegistryProxy.set(this.landRegistry.address);
    (await this.landRegistryProxy.landRegistry.call()).should.be.equal(this.landRegistry.address);

    this.medianizer = await Medianizer.new({ from });
    await this.medianizerProxy.set(this.medianizer.address);
    (await this.medianizerProxy.medianizer.call()).should.be.equal(this.medianizer.address);

    this.whitelist = await Whitelist.new({ from });
    await this.whitelistProxy.set(this.whitelist.address);
    (await this.whitelistProxy.whitelist.call()).should.be.equal(this.whitelist.address);

    await advanceBlock();
  });

  beforeEach(async function () {
    this.token = await TokenizedProperty.new(eGrid, grundstuckNumber, { from });
    await this.landRegistry.tokenizeProperty(eGrid, this.token.address, { from }).should.be.fulfilled;

    this.totalSupply = await this.token.totalSupply.call();

    this.openingTime = (await latestTime()) + duration.weeks(1);
    this.closingTime = this.openingTime + duration.weeks(1);
    this.afterClosingTime = this.closingTime + duration.seconds(1);
    this.tokenSale = await TokenSale.new(this.openingTime, this.closingTime, rate, from, cap, this.token.address, goal, false);
    this.payments = Payments.at(await this.tokenSale.payments());
    await this.whitelist.grantPermission(this.tokenSale.address, authorizedRole);
    await this.token.transfer(this.tokenSale.address, this.totalSupply, { from });
    this.whitelist.grantPermissionBatch([from, wallet, investor, cappedInvestor], authorizedRole);
    this.whitelist.grantPermissionBatch([from, wallet, investor], uncappedRole);
  });

  afterEach(async function () {
    await this.landRegistry.untokenizeProperty(eGrid, { from }).should.be.fulfilled;
  });

  it('should create crowdsale with correct parameters', async function () {
    (await this.tokenSale.openingTime()).should.be.bignumber.equal(this.openingTime);
    (await this.tokenSale.closingTime()).should.be.bignumber.equal(this.closingTime);
    (await this.tokenSale.rate()).should.be.bignumber.equal(new BigNumber(rate));
    (await this.tokenSale.goal()).should.be.bignumber.equal(goal);
    (await this.tokenSale.cap()).should.be.bignumber.equal(cap);
  });

  it('should not accept payments before start', async function () {
    await this.tokenSale.send(ether(1)).should.be.rejectedWith(EVMRevert);
    await this.tokenSale.buyTokens(investor, { from: investor, value: ether(1) }).should.be.rejectedWith(EVMRevert);
  });

  it('should accept payments during the sale', async function () {
    const investmentAmount = ether(1);
    const expectedTokenAmount = rate * investmentAmount;

    await increaseTimeTo(this.openingTime);
    const { logs } = await this.tokenSale.buyTokens(investor, { value: investmentAmount, from: investor }).should.be.fulfilled;
    logs[0].args.purchaser.should.be.equal(investor);
    logs[0].args.beneficiary.should.be.equal(investor);
    logs[0].args.amount.should.be.bignumber.equal(expectedTokenAmount);
  });

  it('should reject payments after end', async function () {
    await increaseTimeTo(this.afterEnd);
    await this.tokenSale.send(ether(1)).should.be.rejectedWith(EVMRevert);
    await this.tokenSale.buyTokens(investor, { value: ether(1), from: investor }).should.be.rejectedWith(EVMRevert);
  });

  it('should allow finalization if hardcap is (approximately) reached', async function () {
    await increaseTimeTo(this.openingTime);
    await this.tokenSale.buyTokens(investor, { value: cap.mul(99).div(100), from: investor });
    await this.tokenSale.finalize({ from }).should.be.fulfilled;
    await this.payments.claim({ from, gasPrice: 0 });
    await this.tokenSale.withdrawTokens({ from: investor });
  });

  it('should only allow investors to withdraw from finalized crowdsales', async function () {
    await increaseTimeTo(this.openingTime);
    await this.tokenSale.buyTokens(investor, { value: cap.mul(999).div(1000), from: investor });
    await this.tokenSale.withdrawTokens({ from: investor }).should.be.rejectedWith(EVMRevert);
  });

  it('should only allow investors to withdraw from succesful crowdsales', async function () {
    await increaseTimeTo(this.openingTime);
    await this.tokenSale.buyTokens(investor, { value: goal.div(2), from: investor });
    await increaseTimeTo(this.afterClosingTime);
    await this.tokenSale.finalize({ from });
    await this.tokenSale.withdrawTokens({ from: investor }).should.be.rejectedWith(EVMRevert);
  });

  it('should reject payments over cap', async function () {
    await increaseTimeTo(this.openingTime);
    await this.tokenSale.send(cap);
    await this.tokenSale.send(1).should.be.rejectedWith(EVMRevert);
  });

  it('should allow finalization and transfer funds to wallet if the goal is reached', async function () {
    await increaseTimeTo(this.openingTime);
    await this.tokenSale.buyTokens(investor, { value: goal, from: investor }).should.be.fulfilled;

    const initialBlockimmoBalance = web3.eth.getBalance(from);
    await increaseTimeTo(this.afterClosingTime);
    await this.tokenSale.finalize({ from, gasPrice: 0 });
    await this.payments.claim({ from, gasPrice: 0 });
    const finalBlockimmoBalance = web3.eth.getBalance(from);
    finalBlockimmoBalance.minus(initialBlockimmoBalance).should.be.bignumber.equal(goal);

    await this.tokenSale.withdrawTokens({ from: investor });
    (await this.token.balanceOf(investor)).should.be.bignumber.equal(this.totalSupply);
  });

  it('should allow refunds if the goal is not reached', async function () {
    const balanceBeforeInvestment = web3.eth.getBalance(investor);

    await increaseTimeTo(this.openingTime);
    await this.tokenSale.sendTransaction({ value: goal.div(2), from: investor, gasPrice: 0 });
    await increaseTimeTo(this.afterClosingTime);
    await this.tokenSale.claimRefund({ from: investor, gasPrice: 0 }).should.be.rejectedWith(EVMRevert);

    await this.tokenSale.finalize({ from });
    await this.tokenSale.claimRefund({ from: investor, gasPrice: 0 }).should.be.fulfilled;

    const balanceAfterRefund = web3.eth.getBalance(investor);
    balanceBeforeInvestment.should.be.bignumber.equal(balanceAfterRefund);

    (await this.token.balanceOf(from)).should.be.bignumber.equal(this.totalSupply);
  });

  it('should not allow refunds if the goal is reached', async function () {
    await increaseTimeTo(this.openingTime);
    await this.tokenSale.sendTransaction({ value: goal, from: investor, gasPrice: 0 });
    await increaseTimeTo(this.afterClosingTime);

    await this.tokenSale.finalize({ from });
    await assertRevert(this.tokenSale.claimRefund({ from: investor, gasPrice: 0 }));

    (await this.token.balanceOf(investor)).should.be.bignumber.equal(0);
  });

  it('only accepts buys with proper whitelist', async function () {
    await increaseTimeTo(this.openingTime);

    await this.tokenSale.buyTokens(unauthorized, { value: minInvestment, from: unauthorized }).should.be.rejected;
    await this.tokenSale.buyTokens(cappedInvestor, { value: cappedLimit, from: cappedInvestor }).should.be.fulfilled;
    await this.tokenSale.buyTokens(cappedInvestor, { value: 1e16, from: cappedInvestor }).should.be.rejected;
    await this.tokenSale.buyTokens(investor, { value: cappedLimit.mul(2), from }).should.be.fulfilled;
    await this.tokenSale.buyTokens(investor, { value: cappedLimit.mul(2), unauthorized }).should.be.fulfilled;
  });

  describe('reversing investments', () => {
    it('should not allow people to call addPayment', async function () {
      await increaseTimeTo(this.openingTime);
      await this.tokenSale.sendTransaction({ value: goal, from: investor, gasPrice: 0 });
      await this.payments.addPayment(from, 1, { from, gasPrice: 0 }).should.be.rejectedWith(EVMRevert);
      await this.payments.addPayment(investor, 1, { investor, gasPrice: 0 }).should.be.rejectedWith(EVMRevert);
    });

    it('should not allow whitelisted accounts to be reversed', async function () {
      await increaseTimeTo(this.openingTime);
      await this.tokenSale.sendTransaction({ value: goal, from: investor, gasPrice: 0 });
      await this.tokenSale.reverse(investor, { from, gasPrice: 0 }).should.be.rejectedWith(EVMRevert);
    });

    it('should not allow accounts to be reversed after finalization', async function () {
      await increaseTimeTo(this.openingTime);
      await this.tokenSale.sendTransaction({ value: goal, from: investor, gasPrice: 0 });
      await increaseTimeTo(this.afterClosingTime);

      await this.whitelist.revokePermission(investor, authorizedRole);
      await this.tokenSale.finalize({ from });
      await this.tokenSale.reverse(investor, { from, gasPrice: 0 }).should.be.rejectedWith(EVMRevert);
    });

    it('should not succeed when funds are rejected', async function () {
      const balanceBeforeInvestment = web3.eth.getBalance(investor);

      await increaseTimeTo(this.openingTime);
      await this.tokenSale.sendTransaction({ value: goal, from: investor, gasPrice: 0 });
      await increaseTimeTo(this.afterClosingTime);

      await this.whitelist.revokePermission(investor, authorizedRole);
      await this.tokenSale.reverse(investor, { from, gasPrice: 0 });
      await this.tokenSale.finalize({ from });
      await this.tokenSale.claimRefund({ from: investor, gasPrice: 0 }).should.be.fulfilled;

      const balanceAfterRefund = web3.eth.getBalance(investor);
      balanceBeforeInvestment.should.be.bignumber.equal(balanceAfterRefund);

      (await this.token.balanceOf(from)).should.be.bignumber.equal(this.totalSupply);
    });

    it('should refund full investment of readded investors when goal not reached', async function () {
      const balanceBeforeInvestment = web3.eth.getBalance(investor);

      await increaseTimeTo(this.openingTime);
      await this.tokenSale.sendTransaction({ value: goal, from: investor, gasPrice: 0 });

      await this.whitelist.revokePermission(investor, authorizedRole);
      await this.tokenSale.reverse(investor, { from, gasPrice: 0 });
      await this.whitelist.grantPermission(investor, authorizedRole);
      await this.tokenSale.sendTransaction({ value: goal.div(2), from: investor, gasPrice: 0 });
      await increaseTimeTo(this.afterClosingTime);

      await this.tokenSale.finalize({ from });
      await this.tokenSale.claimRefund({ from: investor, gasPrice: 0 }).should.be.fulfilled;

      const balanceAfterRefund = web3.eth.getBalance(investor);
      balanceBeforeInvestment.should.be.bignumber.equal(balanceAfterRefund);

      (await this.token.balanceOf(from)).should.be.bignumber.equal(this.totalSupply);
    });

    it('should allow capped investors to reinvest the cap after being readded', async function () {
      await increaseTimeTo(this.openingTime);
      await this.tokenSale.sendTransaction({ value: cappedLimit, from: cappedInvestor, gasPrice: 0 });
      await this.whitelist.revokePermission(cappedInvestor, authorizedRole);
      await this.tokenSale.reverse(cappedInvestor, { from, gasPrice: 0 });
      await this.whitelist.grantPermission(cappedInvestor, authorizedRole);
      await this.tokenSale.sendTransaction({ value: cappedLimit, from: cappedInvestor, gasPrice: 0 });
      await this.tokenSale.sendTransaction({ value: 1, from: cappedInvestor, gasPrice: 0 }).should.be.rejectedWith(EVMRevert);
    });

    it('should allow investment after reversal if cap is no longer reached', async function () {
      await increaseTimeTo(this.openingTime);
      await this.tokenSale.sendTransaction({ value: cap, from: investor, gasPrice: 0 });
      await this.whitelist.revokePermission(investor, authorizedRole);
      await this.tokenSale.reverse(investor, { from, gasPrice: 0 });
      await this.tokenSale.sendTransaction({ value: cap, from: wallet, gasPrice: 0 });
    });

    it('should allow reversal after closing time', async function () {
      await increaseTimeTo(this.openingTime);
      await this.tokenSale.sendTransaction({ value: cap, from: investor, gasPrice: 0 });
      await increaseTimeTo(this.afterClosingTime);
      await this.whitelist.revokePermission(investor, authorizedRole);
      await this.tokenSale.reverse(investor, { from, gasPrice: 0 }).should.be.fulfilled;
    });

    it('should allow refunds for a person removed from whitelist', async function () {
      await increaseTimeTo(this.openingTime);
      const rejectedInvestment = ether(1);
      await this.tokenSale.buyTokens(investor, { value: goal, from: investor }).should.be.fulfilled;
      await this.tokenSale.buyTokens(cappedInvestor, { value: rejectedInvestment, from: cappedInvestor }).should.be.fulfilled;

      const initialBlockimmoBalance = web3.eth.getBalance(from);
      await increaseTimeTo(this.afterClosingTime);

      await this.whitelist.revokePermission(cappedInvestor, authorizedRole, { from, gasPrice: 0 });
      await this.tokenSale.reverse(cappedInvestor, { from, gasPrice: 0 });
      await this.tokenSale.finalize({ from, gasPrice: 0 });
      await this.payments.claim({ from, gasPrice: 0 });

      const finalBlockimmoBalance = web3.eth.getBalance(from);
      finalBlockimmoBalance.minus(initialBlockimmoBalance).should.be.bignumber.equal(goal);

      await this.tokenSale.withdrawTokens({ from: investor, gasPrice: 0 });
      (await this.token.balanceOf(investor)).should.be.bignumber.equal(this.totalSupply);

      const initialRejectBalance = web3.eth.getBalance(cappedInvestor);
      await this.payments.claim({ from: cappedInvestor, gasPrice: 0 });
      const finalRejectBalance = web3.eth.getBalance(cappedInvestor);
      finalRejectBalance.minus(initialRejectBalance).should.be.bignumber.equal(rejectedInvestment);
    });

    it('should allow refunds for multiple people removed from whitelist', async function () {
      await increaseTimeTo(this.openingTime);
      const rejectedInvestment = ether(1);
      await this.tokenSale.buyTokens(investor, { value: goal, from: investor }).should.be.fulfilled;
      await this.tokenSale.buyTokens(cappedInvestor, { value: rejectedInvestment, from: cappedInvestor }).should.be.fulfilled;
      await this.tokenSale.buyTokens(wallet, { value: rejectedInvestment, from: wallet }).should.be.fulfilled;

      const initialBlockimmoBalance = web3.eth.getBalance(from);
      await increaseTimeTo(this.afterClosingTime);

      const rejects = [cappedInvestor, wallet];
      await this.whitelist.revokePermissionBatch(rejects, authorizedRole, { from, gasPrice: 0 });
      await this.tokenSale.reverse(cappedInvestor, { from, gasPrice: 0 });
      await this.tokenSale.reverse(wallet, { from, gasPrice: 0 });
      await this.tokenSale.finalize({ from, gasPrice: 0 });
      await this.payments.claim({ from, gasPrice: 0 });

      const finalBlockimmoBalance = web3.eth.getBalance(from);
      finalBlockimmoBalance.minus(initialBlockimmoBalance).should.be.bignumber.equal(goal);

      await this.tokenSale.withdrawTokens({ from: investor, gasPrice: 0 });
      (await this.token.balanceOf(investor)).should.be.bignumber.equal(this.totalSupply);

      for (const account of rejects) {
        const initialRejectBalance = web3.eth.getBalance(account);
        await this.payments.claim({ from: account, gasPrice: 0 });
        const finalRejectBalance = web3.eth.getBalance(account);
        finalRejectBalance.minus(initialRejectBalance).should.be.bignumber.equal(rejectedInvestment);
      }
    });

    it('should allow readded accounts to receive tokens and ether refund on finalization', async function () {
      await increaseTimeTo(this.openingTime);
      await this.tokenSale.sendTransaction({ value: cappedLimit, from: investor, gasPrice: 0 });
      await this.whitelist.revokePermission(investor, authorizedRole, { from, gasPrice: 0 });
      await this.tokenSale.reverse(investor, { from, gasPrice: 0 });
      await this.whitelist.grantPermission(investor, authorizedRole);
      await this.tokenSale.sendTransaction({ value: goal, from: investor, gasPrice: 0 });

      await increaseTimeTo(this.afterClosingTime);
      await this.tokenSale.finalize({ from });

      await this.tokenSale.withdrawTokens({ from: investor });
      (await this.token.balanceOf(investor)).should.be.bignumber.equal(this.totalSupply);

      const initialRejectBalance = web3.eth.getBalance(investor);
      await this.payments.claim({ from: investor, gasPrice: 0 });
      const finalRejectBalance = web3.eth.getBalance(investor);
      finalRejectBalance.minus(initialRejectBalance).should.be.bignumber.equal(cappedLimit);
    });

    it('should allow extra ether to be distributed', async function () {
      await increaseTimeTo(this.openingTime);
      const rejectedInvestment = ether(1);
      await this.tokenSale.buyTokens(investor, { value: goal, from: investor }).should.be.fulfilled;
      await this.tokenSale.buyTokens(cappedInvestor, { value: rejectedInvestment, from: cappedInvestor }).should.be.fulfilled;
      await this.tokenSale.buyTokens(wallet, { value: rejectedInvestment, from: wallet }).should.be.fulfilled;

      const initialBlockimmoBalance = web3.eth.getBalance(from);
      await increaseTimeTo(this.afterClosingTime);

      const rejects = [cappedInvestor, wallet];
      await this.whitelist.revokePermissionBatch(rejects, authorizedRole, { from, gasPrice: 0 });
      await this.payments.sendTransaction({ value: goal.div(2), from: unauthorized, gasPrice: 0 });
      await this.tokenSale.reverse(cappedInvestor, { from, gasPrice: 0 });
      await this.tokenSale.reverse(wallet, { from, gasPrice: 0 });
      await this.tokenSale.finalize({ from, gasPrice: 0 });
      await this.payments.claim({ from, gasPrice: 0 });

      const finalBlockimmoBalance = web3.eth.getBalance(from);
      assert(finalBlockimmoBalance.minus(initialBlockimmoBalance) > goal, 'blockimmo should receive extra ether');

      await this.tokenSale.withdrawTokens({ from: investor, gasPrice: 0 });
      (await this.token.balanceOf(investor)).should.be.bignumber.equal(this.totalSupply);

      for (const account of rejects) {
        const initialRejectBalance = web3.eth.getBalance(account);
        await this.payments.claim({ from: account, gasPrice: 0 });
        const finalRejectBalance = web3.eth.getBalance(account);
        assert(finalRejectBalance.minus(initialRejectBalance) > rejectedInvestment);
      }
    });
  });

  describe('when goal > cap', () => {
    const lowCap = ether(1);
    const highGoal = ether(2);
    it('creation reverts', async function () {
      await (TokenSale.new(this.openingTime, this.closingTime, rate, wallet, lowCap, this.token.address, highGoal, false)).should.be.rejected;
    });
  });

  describe('stable (usd)', () => {
    const usdGoal = new BigNumber(10000);
    const weiGoal = usdGoal.mul(1e18).mul(1e18).div(conversionRate);

    beforeEach(async function () {
      this.token = await TokenizedProperty.new('eGrid', grundstuckNumber, { from });
      await this.landRegistry.tokenizeProperty('eGrid', this.token.address, { from }).should.be.fulfilled;

      this.totalSupply = await this.token.totalSupply.call();
      this.tokenSale = await TokenSale.new(this.openingTime, this.closingTime, rate, from, usdGoal.mul(100), this.token.address, usdGoal, true);
      this.payments = Payments.at(await this.tokenSale.payments());
      await this.whitelist.grantPermission(this.tokenSale.address, authorizedRole);
      await this.token.transfer(this.tokenSale.address, this.totalSupply, { from });
    });

    afterEach(async function () {
      await this.landRegistry.untokenizeProperty('eGrid', { from }).should.be.fulfilled;
    });

    it('goal reached', async function () {
      await increaseTimeTo(this.openingTime);
      await this.tokenSale.buyTokens(investor, { value: weiGoal, from: investor }).should.be.fulfilled;

      const beforeFinalization = web3.eth.getBalance(from);
      await increaseTimeTo(this.afterClosingTime);
      await this.tokenSale.finalize({ from, gasPrice: 0 }).should.be.fulfilled;
      await this.payments.claim({ from, gasPrice: 0 });
      const afterFinalization = web3.eth.getBalance(from);

      assert(Math.abs((afterFinalization - beforeFinalization) - weiGoal) <= 1e8);

      await this.tokenSale.withdrawTokens({ from: investor });
      (await this.token.balanceOf(investor)).should.be.bignumber.equal(this.totalSupply);
    });

    it('goal not reached', async function () {
      await increaseTimeTo(this.openingTime);
      await this.tokenSale.buyTokens(investor, { value: weiGoal.div(2), from: investor }).should.be.fulfilled;

      const beforeFinalization = web3.eth.getBalance(from);
      await increaseTimeTo(this.afterClosingTime);
      await this.tokenSale.finalize({ from, gasPrice: 0 });
      const afterFinalization = web3.eth.getBalance(from);

      afterFinalization.minus(beforeFinalization).should.be.bignumber.equal(0);

      await assertRevert(this.tokenSale.withdrawTokens({ from: investor }));
      (await this.token.balanceOf(investor)).should.be.bignumber.equal(0);
    });

    it('only accepts buys with proper whitelist', async function () {
      await increaseTimeTo(this.openingTime);

      await this.tokenSale.buyTokens(unauthorized, { value: 1, from: unauthorized }).should.be.rejected;
      await this.tokenSale.buyTokens(cappedInvestor, { value: cappedLimit, from: cappedInvestor }).should.be.fulfilled;
      await this.tokenSale.buyTokens(cappedInvestor, { value: 1e16, from: cappedInvestor }).should.be.rejected;
      await this.tokenSale.buyTokens(from, { value: cappedLimit.mul(2), from }).should.be.fulfilled;
      await this.tokenSale.buyTokens(from, { value: cappedLimit.mul(2), unauthorized }).should.be.fulfilled;
    });

    it('can upgrade medianizer mid-crowdsale', async function () {
      await increaseTimeTo(this.openingTime);

      const newMedianizer = await Medianizer.new({ from });
      await this.medianizerProxy.set(newMedianizer.address);
      (await this.medianizerProxy.medianizer.call()).should.be.equal(newMedianizer.address);
    });
  });
});
