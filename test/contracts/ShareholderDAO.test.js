import { advanceBlock } from 'openzeppelin-solidity/test/helpers/advanceToBlock';
import { assertRevert } from 'openzeppelin-solidity/test/helpers/assertRevert';
import { expectThrow } from 'openzeppelin-solidity/test/helpers/expectThrow';
import { latestTime } from 'openzeppelin-solidity/test/helpers/latestTime';
const { increaseTimeTo, duration } = require('openzeppelin-solidity/test/helpers/increaseTime');

const BigNumber = web3.BigNumber;

const should = require('chai')
  .use(require('chai-as-promised'))
  .use(require('chai-bignumber')(BigNumber))
  .should();

const LandRegistry = artifacts.require('LandRegistry');
const LandRegistryProxy = artifacts.require('LandRegistryProxy');
const MedianizerProxy = artifacts.require('MedianizerProxy');
const ShareholderDAO = artifacts.require('ShareholderDAO');
const TokenizedProperty = artifacts.require('TokenizedProperty');
const Whitelist = artifacts.require('Whitelist');
const WhitelistProxy = artifacts.require('WhitelistProxy');

const Medianizer = artifacts.require('Medianizer');

contract('ShareholderDAO', ([blockimmo, account1, account2, account3, account4, from]) => {
  const eGrid = 'CH327417066524';
  const grundstuckNumber = '566';

  before(async function () {
    this.landRegistryProxy = await LandRegistryProxy.new({ from: blockimmo });
    this.landRegistryProxy.address.should.be.equal('0x0f5ea0a652e851678ebf77b69484bfcd31f9459b');

    this.medianizerProxy = await MedianizerProxy.new({ from: blockimmo });
    this.medianizerProxy.address.should.be.equal('0xec8be1a5630364292e56d01129e8ee8a9578d7d8');

    this.whitelistProxy = await WhitelistProxy.new({ from: blockimmo });
    this.whitelistProxy.address.should.be.equal('0x85a84691547b7ccf19d7c31977a7f8c0af1fb25a');

    this.landRegistry = await LandRegistry.new({ from: blockimmo });
    await this.landRegistryProxy.set(this.landRegistry.address);
    (await this.landRegistryProxy.landRegistry.call()).should.be.equal(this.landRegistry.address);

    this.medianizer = await Medianizer.new({ from: blockimmo });
    await this.medianizerProxy.set(this.medianizer.address);
    (await this.medianizerProxy.medianizer.call()).should.be.equal(this.medianizer.address);

    this.whitelist = await Whitelist.new({ from: blockimmo });
    await this.whitelistProxy.set(this.whitelist.address);
    (await this.whitelistProxy.whitelist.call()).should.be.equal(this.whitelist.address);

    const authorizedRole = 'authorized';
    await this.whitelist.grantPermissionBatch([blockimmo, account1, account2, account3, account4, from], authorizedRole, { from: blockimmo });
    await advanceBlock();
  });

  beforeEach(async function () {
    this.token = await TokenizedProperty.new(eGrid, grundstuckNumber, { from });
    await this.landRegistry.tokenizeProperty(eGrid, this.token.address, { from: blockimmo });

    this.totalSupply = await this.token.totalSupply.call();

    await this.token.transfer(account1, this.totalSupply / 4, { from });
    await this.token.transfer(account2, this.totalSupply / 4, { from });
    await this.token.transfer(account3, this.totalSupply / 4, { from });

    this.contract = await ShareholderDAO.new(this.token.address, { from });
    await this.token.transferOwnership(this.contract.address, { from });

    this.closingTime = (await latestTime()) + duration.weeks(1);
  });

  afterEach(async function () {
    await this.landRegistry.untokenizeProperty(eGrid, { from: blockimmo }).should.be.fulfilled;
  });

  describe('extendProposal', () => {
    it('extend a proposal', async function () {
      const { logs } = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      logs.length.should.be.equal(1);
      logs[0].event.should.be.equal('ProposalExtended');
      logs[0].args.managementCompany.should.be.equal('dwk');
      logs[0].args.closingTime.toNumber().should.be.equal(this.closingTime);
      parseInt(logs[0].args.owner).should.be.equal(0x0);
      logs[0].args.generic.should.be.equal('');
      logs[0].args.proposer.should.be.equal(from);
    });

    it('unauthorized', async function () {
      await assertRevert(this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from: account4 }));
    });

    it('blockimmo is authorized', async function () {
      const { logs } = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from: blockimmo });
      logs[0].event.should.be.equal('ProposalExtended');
    });

    it('invalid closing time', async function () {
      const time = (await latestTime()) - duration.seconds(1);
      await assertRevert(this.contract.extendProposal(0, time, 'dwk', 0x0, '', { from: account4 }));
    });

    it('duplicate', async function () {
      await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      await assertRevert(this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from }));
    });

    it('extend multiple proposals', async function () {
      await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      await this.contract.extendProposal(0, this.closingTime, 'dwk-different', 0x0, '', { from });
      await this.contract.extendProposal(1, this.closingTime, '', account4, '', { from: account1 });
    });

    it('invalid action', async function () {
      await expectThrow(this.contract.extendProposal(4, this.closingTime, '', 0x0, '', { from }));
    });
  });

  describe('vote', () => {
    it('vote for a proposal', async function () {
      const r = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;

      const { logs } = await this.contract.vote(proposal, true, { from });
      logs.length.should.be.equal(1);
      logs[0].event.should.be.equal('Voted');
      logs[0].args.proposal.should.be.equal(proposal);
      logs[0].args.voter.should.be.equal(from);
    });

    it('unauthorized', async function () {
      const { logs } = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = logs[0].args.proposal;

      await assertRevert(this.contract.vote(proposal, true, { from: account4 }));
    });

    it('blockimmo authorized', async function () {
      const r = await this.contract.extendProposal(2, this.closingTime, '', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;

      const { logs } = await this.contract.vote(proposal, true, { from: blockimmo });
      logs.length.should.be.equal(1);
      logs[0].event.should.be.equal('Voted');
      logs[0].args.proposal.should.be.equal(proposal);
      logs[0].args.voter.should.be.equal(blockimmo);
      logs[0].args.clout.toNumber().should.be.equal(0);
    });

    it('closed', async function () {
      const { logs } = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = logs[0].args.proposal;

      await increaseTimeTo(this.closingTime + duration.seconds(1));
      await assertRevert(this.contract.vote(proposal, true, { from }));
    });

    it('proposal does not exist', async function () {
      await assertRevert(this.contract.vote('0x7afb02a7b4f8dffc6e0f6859b3183f482cf4dcaa909f3506b70b6c64c662739d', true, { from }));
    });

    it('already voted', async function () {
      const { logs } = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = logs[0].args.proposal;

      await this.contract.vote(proposal, true, { from });
      await assertRevert(this.contract.vote(proposal, true, { from }));
    });
  });

  describe('rescindVote', () => {
    it('rescind a vote (for)', async function () {
      const r = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;

      await this.contract.vote(proposal, true, { from });
      const { logs } = await this.contract.rescindVote(proposal, { from });
      logs.length.should.be.equal(1);
      logs[0].event.should.be.equal('VoteRescinded');
      logs[0].args.proposal.should.be.equal(proposal);
      logs[0].args.voter.should.be.equal(from);

      await this.contract.vote(proposal, true, { from }).should.be.fulfilled;
    });

    it('rescind a vote (against)', async function () {
      const { logs } = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = logs[0].args.proposal;

      await this.contract.vote(proposal, false, { from });
      await this.contract.rescindVote(proposal, { from });

      await this.contract.vote(proposal, true, { from }).should.be.fulfilled;
    });

    it('can change outcome by rescinding', async function () {
      const r = await this.contract.extendProposal(3, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;

      await this.contract.vote(proposal, true, { from });
      await this.contract.vote(proposal, true, { from: account1 });
      await this.contract.vote(proposal, true, { from: account2 });
      await this.contract.vote(proposal, true, { from: account3 });
      await this.contract.rescindVote(proposal, { from: account1 });
      await this.contract.rescindVote(proposal, { from: account2 });
      await increaseTimeTo(this.closingTime + duration.seconds(1));
      const { logs } = await this.contract.finalize(proposal, { from });

      logs[0].event.should.be.equal('ProposalRejected');
    });

    it('can not rescind more votes than it cast', async function () {
      await this.token.transfer(account4, this.totalSupply / 8, { from: account2 });
      // account2 and account4 each have 1/8 of total supply
      const r = await this.contract.extendProposal(3, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;

      await this.contract.vote(proposal, true, { from });
      await this.contract.vote(proposal, true, { from: account1 });
      await this.contract.vote(proposal, true, { from: account2 });
      await this.contract.vote(proposal, true, { from: account4 });
      // total vote in favor is 1/4 + 1/4 + 1/8 + 1/8 = 3/4
      await this.token.transfer(account2, this.totalSupply / 4, { from: account3 });
      await this.token.transfer(account2, this.totalSupply / 8, { from: account4 });
      // account2's supply increases to 1/2
      await this.contract.rescindVote(proposal, { from: account2 });
      // If rescinding vote changed count on current balance, proposal would be rejected
      // This would be because the tally in support would decrease from 3/4 to 1/4.
      // Instead, it only decreases by the amount the tokenholder had while voting.
      // Therefore, it only decreases from 3/4 to 5/8 of the total supply.
      await increaseTimeTo(this.closingTime + duration.seconds(1));
      const { logs } = await this.contract.finalize(proposal, { from });

      logs[0].event.should.be.equal('ProposalExecuted');
    });
    it('blockimmo authorized', async function () {
      const r = await this.contract.extendProposal(2, this.closingTime, '', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;

      await this.contract.vote(proposal, true, { from: blockimmo });
      const { logs } = await this.contract.rescindVote(proposal, { from: blockimmo });
      logs.length.should.be.equal(1);
      logs[0].event.should.be.equal('VoteRescinded');
      logs[0].args.proposal.should.be.equal(proposal);
      logs[0].args.voter.should.be.equal(blockimmo);
      logs[0].args.clout.toNumber().should.be.equal(0);

      await this.contract.vote(proposal, true, { from: blockimmo }).should.be.fulfilled;
    });

    it('proposal does not exist', async function () {
      await assertRevert(this.contract.rescindVote('0x7afb02a7b4f8dffc6e0f6859b3183f482cf4dcaa909f3506b70b6c64c662739d', { from }));
    });

    it('closed', async function () {
      const { logs } = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = logs[0].args.proposal;

      await this.contract.vote(proposal, true, { from });

      await increaseTimeTo(this.closingTime + duration.seconds(1));
      await assertRevert(this.contract.vote(proposal, true, { from: account1 }));
    });

    it('has not voted', async function () {
      const { logs } = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      await assertRevert(this.contract.rescindVote(logs[0].args.proposal, { from }));
    });
  });

  describe('finalize', () => {
    it('execute a proposal', async function () {
      const r = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;

      await this.contract.vote(proposal, true, { from });
      await this.contract.vote(proposal, true, { from: account1 });
      await this.contract.vote(proposal, true, { from: account2 });

      await increaseTimeTo(this.closingTime + duration.seconds(1));
      const { logs } = await this.contract.finalize(proposal, { from });
      logs.length.should.be.equal(1);
      logs[0].event.should.be.equal('ProposalExecuted');
      logs[0].args.proposal.should.be.equal(proposal);

      const managementCompany = await this.token.managementCompany.call();
      assert.equal('dwk', managementCompany);
    });

    it('reject a proposal', async function () {
      const r = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;

      await this.contract.vote(proposal, false, { from });
      await this.contract.vote(proposal, false, { from: account1 });

      await increaseTimeTo(this.closingTime + duration.seconds(1));
      const { logs } = await this.contract.finalize(proposal, { from });
      logs.length.should.be.equal(1);
      logs[0].event.should.be.equal('ProposalRejected');
      logs[0].args.proposal.should.be.equal(proposal);

      const managementCompany = await this.token.managementCompany.call();
      assert.equal('', managementCompany);
    });

    it('reject a proposal by not voting', async function () {
      const r = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;

      await increaseTimeTo(this.closingTime + duration.seconds(1));
      const { logs } = await this.contract.finalize(proposal, { from });
      logs[0].event.should.be.equal('ProposalRejected');
    });

    it('transfer ownership', async function () {
      const { logs } = await this.contract.extendProposal(1, this.closingTime, '', account4, '', { from });
      const proposal = logs[0].args.proposal;

      await this.contract.vote(proposal, true, { from });
      await this.contract.vote(proposal, true, { from: account1 });
      await this.contract.vote(proposal, true, { from: account2 });

      await increaseTimeTo(this.closingTime + duration.seconds(1));
      await this.contract.finalize(proposal, { from: account1 });

      const owner = await this.token.owner.call();
      assert.equal(account4, owner);
    });

    it('untokenize', async function () {
      const { logs } = await this.contract.extendProposal(2, this.closingTime, '', 0x0, '', { from });
      const proposal = logs[0].args.proposal;

      await this.contract.vote(proposal, true, { from });
      await this.contract.vote(proposal, true, { from: account1 });
      await this.contract.vote(proposal, true, { from: account2 });

      await increaseTimeTo(this.closingTime + duration.seconds(1));
      await this.contract.finalize(proposal, { from: account1 }).should.be.fulfilled;
    });

    it('generic', async function () {
      const generic = 'hello blockimmo';
      const { logs } = await this.contract.extendProposal(3, this.closingTime, '', 0x0, generic, { from });
      logs[0].args.generic.should.be.equal(generic);
      const { proposal } = logs[0].args;

      await this.contract.vote(proposal, true, { from });
      await this.contract.vote(proposal, true, { from: account1 });
      await this.contract.vote(proposal, true, { from: account2 });

      await increaseTimeTo(this.closingTime + duration.seconds(1));
      await this.contract.finalize(proposal, { from: account1 }).should.be.fulfilled;
    });

    it('unauthorized', async function () {
      const { logs } = await this.contract.extendProposal(2, this.closingTime, '', 0x0, '', { from });
      const proposal = logs[0].args.proposal;

      await increaseTimeTo(this.closingTime + duration.seconds(1));
      await assertRevert(this.contract.finalize(proposal, { from: account4 }));
    });

    it('blockimmo authorized', async function () {
      const { logs } = await this.contract.extendProposal(2, this.closingTime, '', 0x0, '', { from });
      const proposal = logs[0].args.proposal;

      await increaseTimeTo(this.closingTime + duration.seconds(1));
      await this.contract.finalize(proposal, { from: blockimmo }).should.be.fulfilled;
    });

    it('not closed', async function () {
      const { logs } = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = logs[0].args.proposal;

      await assertRevert(this.contract.finalize(proposal, { from }));
    });

    it('proposal does not exist', async function () {
      await assertRevert(this.contract.finalize('0x7afb02a7b4f8dffc6e0f6859b3183f482cf4dcaa909f3506b70b6c64c662739d', { from }));
    });
  });

  describe('integration tests', () => {
    it('can transfer after extending a proposal', async function () {
      await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      await this.token.transfer(account1, this.totalSupply / 8, { from }).should.be.fulfilled;
    });

    it('can receive tokens when actively voting', async function () {
      const r = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;

      await this.contract.vote(proposal, true, { from });
      const { logs } = await this.token.transfer(from, this.totalSupply / 4, { from: account1 });
      logs[0].event.should.be.equal('Transfer');
    });

    it('can transfer tokens when actively voting', async function () {
      const r = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;

      await this.contract.vote(proposal, true, { from });
      const { logs } = await this.token.transfer(account4, this.totalSupply / 4, { from });
      logs[0].event.should.be.equal('Transfer');
    });

    it('can not revote', async function () {
      const r = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;

      await this.contract.vote(proposal, true, { from });
      await assertRevert(this.contract.vote(proposal, true, { from }));
    });

    it('can not revote by transfering tokens', async function () {
      const r = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;

      await this.contract.vote(proposal, true, { from });
      const { logs } = await this.token.transfer(account4, this.totalSupply / 4, { from });
      await assertRevert(this.contract.vote(proposal, true, { from: account4 }));
    });

    it('can not revote by using accept/transferFrom to move tokens', async function () {
      const r = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;

      await this.contract.vote(proposal, true, { from });
      await this.token.approve(from, this.totalSupply / 4, { from });
      await this.token.transferFrom(from, account4, this.totalSupply / 4, { from });
      await assertRevert(this.contract.vote(proposal, true, { from: account4 }));
    });

    it('can not vote on proposals raised before most recent transfer', async function () {
      const r = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;

      await this.token.transfer(account4, this.totalSupply / 4, { from });
      await assertRevert(this.contract.vote(proposal, true, { from }));
    });

    it('can rescind votes even after transfer', async function () {
      const r = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;

      await this.contract.vote(proposal, true, { from });
      await this.token.transfer(account4, 1, { from });
      const { logs } = await this.contract.rescindVote(proposal, { from });
      logs[0].event.should.be.equal('VoteRescinded');
    });

    it('can not revote after a transfer', async function () {
      const r = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;

      await this.contract.vote(proposal, true, { from });
      await this.token.transfer(account4, 1, { from });
      await this.contract.rescindVote(proposal, { from });
      await assertRevert(this.contract.vote(proposal, true, { from }));
    });

    it('can transfer once executed', async function () {
      const { logs } = await this.contract.extendProposal(1, this.closingTime, '', account4, '', { from });
      const proposal = logs[0].args.proposal;

      await this.contract.vote(proposal, true, { from });
      await this.contract.vote(proposal, true, { from: account1 });
      await this.contract.vote(proposal, true, { from: account2 });

      await increaseTimeTo(this.closingTime + duration.seconds(1));
      await this.contract.finalize(proposal, { from: account2 }).should.be.fulfilled;

      await this.token.transfer(account1, this.totalSupply / 4, { from });
    });

    it('can transfer once rejected', async function () {
      const { logs } = await this.contract.extendProposal(1, this.closingTime, '', account4, '', { from });
      const proposal = logs[0].args.proposal;

      await this.contract.vote(proposal, false, { from });
      await this.contract.vote(proposal, false, { from: account1 });

      await increaseTimeTo(this.closingTime + duration.seconds(1));
      await this.contract.finalize(proposal, { from }).should.be.fulfilled;

      await this.token.transfer(account1, this.totalSupply / 4, { from });
    });

    it('can have attackers prevent votes through trivial transfers', async function () {
      const r = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;

      this.token.transfer(from, 1, { from: account1 });
      // Attackers sends a tiny amount of token to prevent a large stakeholder's voting
      await assertRevert(this.contract.vote(proposal, true, { from }));
      // Reverts when the account tries to vote, since the transaction was too recent
    });

    it('can have attackers prevent votes through trivial trasferFroms', async function () {
      const r = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;
      // Same as above, just shows an annoything thing someone can do
      await this.token.approve(account1, 1, { from: account1 });
      await this.token.transferFrom(account1, from, 1, { from: account1 });
      await assertRevert(this.contract.vote(proposal, true, { from }));
    });

    it('can prevent transfers from being used to block votes', async function () {
      const r = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const proposal = r.logs[0].args.proposal;

      this.token.setMinTransfer(1e8, { from });

      await this.contract.vote(proposal, true, { from });
      await assertRevert(this.token.transfer(from, 1, { from: account1 }));
      // attacker tries sending a tiny amount of token to prevent voting
      // Should revert since account protects
      const { logs } = await this.contract.vote(proposal, false, { from: account1 });
      logs[0].event.should.be.equal('Voted');
    });

    it('will allow transfers when account has open proposals in previous DAO', async function () {
      const r = await this.contract.extendProposal(0, this.closingTime, 'dwk', 0x0, '', { from });
      const earlyProposal = r.logs[0].args.proposal;
      await this.contract.vote(earlyProposal, true, { from });
      const r2 = await this.contract.extendProposal(1, this.closingTime, '', account4, '', { from });

      const proposal = r2.logs[0].args.proposal;

      await this.contract.vote(proposal, true, { from });
      await this.contract.vote(proposal, true, { from: account1 });
      await this.contract.vote(proposal, true, { from: account2 });

      await increaseTimeTo(this.closingTime + duration.seconds(1));
      await this.contract.finalize(proposal, { from: account1 });

      const owner = await this.token.owner.call();
      assert.equal(account4, owner);

      const { logs } = await this.token.transfer(from, this.totalSupply / 4, { from: account1 });
      logs[0].event.should.be.equal('Transfer');
    });
  });
});
