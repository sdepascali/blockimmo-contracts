import { assertRevert } from 'openzeppelin-solidity/test/helpers/assertRevert';

const BigNumber = web3.BigNumber;

const should = require('chai')
  .use(require('chai-as-promised'))
  .use(require('chai-bignumber')(BigNumber))
  .should();

const LandRegistry = artifacts.require('LandRegistry');
const LandRegistryProxy = artifacts.require('LandRegistryProxy');
const MedianizerProxy = artifacts.require('MedianizerProxy');
const TokenizedProperty = artifacts.require('TokenizedProperty');
const Whitelist = artifacts.require('Whitelist');
const WhitelistProxy = artifacts.require('WhitelistProxy');

const Medianizer = artifacts.require('Medianizer');

contract('TokenizedProperty', ([from, other, unauthorized]) => {
  const eGrid = 'CH327417066524';
  const grundstuckNumber = 'ZG-566';
  const authorizedRole = 'authorized';

  before(async function () {
    this.landRegistryProxy = await LandRegistryProxy.new({ from });
    this.landRegistryProxy.address.should.be.equal('0x0f5ea0a652e851678ebf77b69484bfcd31f9459b');

    this.medianizerProxy = await MedianizerProxy.new({ from });
    this.medianizerProxy.address.should.be.equal('0xec8be1a5630364292e56d01129e8ee8a9578d7d8');

    this.whitelistProxy = await WhitelistProxy.new({ from });
    this.whitelistProxy.address.should.be.equal('0x85a84691547b7ccf19d7c31977a7f8c0af1fb25a');

    this.landRegistry = await LandRegistry.new({ from });
    await this.landRegistryProxy.set(this.landRegistry.address);
    (await this.landRegistryProxy.landRegistry.call()).should.be.equal(this.landRegistry.address);

    this.medianizer = await Medianizer.new({ from });
    await this.medianizerProxy.set(this.medianizer.address);
    (await this.medianizerProxy.medianizer.call()).should.be.equal(this.medianizer.address);

    this.whitelist = await Whitelist.new({ from });
    await this.whitelistProxy.set(this.whitelist.address);
    (await this.whitelistProxy.whitelist.call()).should.be.equal(this.whitelist.address);

    await this.whitelist.grantPermissionBatch([from, other], authorizedRole);
  });

  beforeEach(async function () {
    this.tokenizeProperty = await TokenizedProperty.new(eGrid, grundstuckNumber, { from });
    await this.landRegistry.tokenizeProperty(eGrid, this.tokenizeProperty.address, { from }).should.be.fulfilled;

    this.numTokens = await this.tokenizeProperty.NUM_TOKENS.call();
    this.decimals = await this.tokenizeProperty.decimals.call();
  });

  afterEach(async function () {
    await this.landRegistry.untokenizeProperty(eGrid, { from }).should.be.fulfilled;
  });

  describe('tokenizeProperty', () => {
    it('tokenize a property', async function () {
      const balance = await this.tokenizeProperty.balanceOf.call(from);
      (this.numTokens * (10 ** this.decimals)).should.be.bignumber.equal(balance);
      (await this.tokenizeProperty.name.call()).should.be.equal(eGrid);
      (await this.tokenizeProperty.symbol.call()).should.be.equal(grundstuckNumber);
      (await this.tokenizeProperty.managementCompany.call()).should.be.equal('');
    });

    it('set the management company', async function () {
      const managementCompany = 'blockimmo';

      const { logs } = await this.tokenizeProperty.setManagementCompany(managementCompany, { from });
      logs.length.should.be.equal(1);
      logs[0].event.should.be.equal('ManagementCompanySet');
      logs[0].args.managementCompany.should.be.equal(managementCompany);

      (await this.tokenizeProperty.managementCompany.call()).should.be.equal(managementCompany);
    });

    it('only the owner can set the management company', async function () {
      await assertRevert(this.tokenizeProperty.setManagementCompany('blockimmo', { from: other }));
    });

    it('transfer', async function () {
      const amount = 1000 * (10 ** this.decimals);

      const { logs } = await this.tokenizeProperty.transfer(other, amount, { from });
      logs.length.should.be.equal(1);
      logs[0].event.should.be.equal('Transfer');
      logs[0].args.from.should.be.equal(from);
      logs[0].args.to.should.be.equal(other);
      logs[0].args.value.should.be.bignumber.equal(amount);

      const balance = await this.tokenizeProperty.balanceOf.call(other);
      balance.should.be.bignumber.equal(amount);
    });

    it('cannot transfer to unauthorized', async function () {
      const amount = 1000 * (10 ** this.decimals);
      const { logs } = await this.tokenizeProperty.transfer(unauthorized, amount, { from }).should.be.rejected;
    });

    it('can transfer even after losing authorization', async function () {
      const amount = 1000 * (10 ** this.decimals);
      await this.whitelist.revokePermission(from, authorizedRole, { from });
      const { logs } = await this.tokenizeProperty.transfer(other, amount, { from }).should.be.fulfilled;
    });

    it('untokenize request', async function () {
      const { logs } = await this.tokenizeProperty.untokenize({ from });
      assert.equal(logs.length, 1);
      assert.equal(logs[0].event, 'UntokenizeRequest');
    });

    it('cannot set the management company when untokenized', async function () {
      await this.landRegistry.untokenizeProperty(eGrid).should.be.fulfilled;
      await assertRevert(this.tokenizeProperty.setManagementCompany('blockimmo', { from }));

      await this.landRegistry.tokenizeProperty(eGrid, this.tokenizeProperty.address, { from }).should.be.fulfilled;
    });

    it('cannot transfer when untokenized', async function () {
      await this.landRegistry.untokenizeProperty(eGrid).should.be.fulfilled;
      await assertRevert(this.tokenizeProperty.transfer(other, 1000 * (10 ** this.decimals), { from }));

      await this.landRegistry.tokenizeProperty(eGrid, this.tokenizeProperty.address, { from }).should.be.fulfilled;
    });

    it('can set a minimum number of tokens to be transferred', async function () {
      await this.tokenizeProperty.setMinTransfer(1e8, { from: other });
      await assertRevert(this.tokenizeProperty.transfer(other, 1e8 - 1, { from }));
      const { logs } = await this.tokenizeProperty.transfer(other, 1e8, { from });
      logs[0].event.should.be.equal('Transfer');
      const minAmount = await this.tokenizeProperty.minTransferAccepted(other, { from: other });
      minAmount.should.be.bignumber.equal(1e8);
    });

    it('tracks the block numbers of the last transfers', async function () {
      await this.tokenizeProperty.transfer(other, 1, { from });
      const { number } = await web3.eth.getBlock('latest');
      const ltb = await this.tokenizeProperty.lastTransferBlock(from, { from });
      number.should.be.bignumber.equal(ltb);
    });
  });
});
