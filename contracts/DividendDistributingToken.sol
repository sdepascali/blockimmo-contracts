pragma solidity 0.4.25;

import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/token/ERC20/StandardToken.sol";


/**
 * @title DividendDistributingToken
 * @dev An ERC20-compliant token that distributes any Ether it receives to its token holders proportionate to their share.
 *
 * Implementation exactly based on: https://blog.pennyether.com/posts/realtime-dividend-token.html#the-token
 *
 * The user is responsible for when they transact tokens (transacting before a dividend payout is probably not ideal).
 *
 * `TokenizedProperty` inherits from `this` and is the front-facing contract representing the rights / ownership to a property.
 */
contract DividendDistributingToken is StandardToken {
  using SafeMath for uint256;

  uint256 public constant POINTS_PER_WEI = uint256(10) ** 32;

  uint256 public pointsPerToken = 0;
  mapping(address => uint256) public credits;
  mapping(address => uint256) public lastPointsPerToken;

  event DividendsDeposited(address indexed payer, uint256 amount);
  event DividendsCollected(address indexed collector, uint256 amount);

  function collectOwedDividends() public {
    creditAccount(msg.sender);

    uint256 _wei = credits[msg.sender] / POINTS_PER_WEI;

    credits[msg.sender] = 0;

    msg.sender.transfer(_wei);
    emit DividendsCollected(msg.sender, _wei);
  }

  function creditAccount(address _account) internal {
    uint256 amount = balanceOf(_account).mul(pointsPerToken.sub(lastPointsPerToken[_account]));
    credits[_account] = credits[_account].add(amount);
    lastPointsPerToken[_account] = pointsPerToken;
  }

  function deposit(uint256 _value) internal {
    pointsPerToken = pointsPerToken.add(_value.mul(POINTS_PER_WEI) / totalSupply_);
    emit DividendsDeposited(msg.sender, _value);
  }
}
