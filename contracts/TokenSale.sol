pragma solidity 0.4.25;

import "openzeppelin-solidity/contracts/crowdsale/distribution/RefundableCrowdsale.sol";
import "openzeppelin-solidity/contracts/crowdsale/distribution/PostDeliveryCrowdsale.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/payment/SplitPayment.sol";
import "openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "openzeppelin-solidity/contracts/token/ERC20/SafeERC20.sol";


contract MedianizerInterface {
  function read() public view returns (bytes32);
}


contract MedianizerProxyInterface {
  function medianizer() public view returns (MedianizerInterface);
}


contract WhitelistInterface {
  function checkRole(address _operator, string _role) public view;
  function hasRole(address _operator, string _role) public view returns (bool);
}


contract WhitelistProxyInterface {
  function whitelist() public view returns (WhitelistInterface);
}


contract Payments is Ownable, SplitPayment {
  constructor() public SplitPayment(new address[](0), new uint256[](0)) { }

  function addPayment(address _payee, uint256 _amount) public onlyOwner {
    addPayee(_payee, _amount);
  }
}


/**
 * @title TokenSale
 * @dev Distribute tokens to investors in exchange for Ether.
 *
 * This is the primary mechanism for outright sales of commercial investment properties (and blockimmo's STO, where shares
 * of our company are represented as `TokenizedProperty`) (official pending FINMA approval).
 *
 * Selling:
 *   1. Deploy `TokenizedProperty`. Initially all tokens and ownership of this property will be assigned to the 'deployer'
 *   2. Deploy `ShareholderDAO` and transfer the property's (1) ownership to it
 *   3. Configure and deploy a `TokenSale`
 *     - After completing (1, 2, 3) blockimmo will verify the property as legitimate in `LandRegistry`
 *     - blockimmo will then authorize `this` to the `Whitelist` before seller can proceed to (4)
 *   4. Transfer tokens of `TokenizedProperty` (1) to be sold to `this` (3)
 *   5. Investors are able to buy tokens while the sale is open. 'Deployer' calls `finalize` to complete the sale
 *
 * Note: blockimmo will be responsible for managing initial sales on our platform. This means we will be configuring
 *       and deploying all contracts for sellers. This provides an extra layer of control/security until we've refined
 *       these processes and proven them in the real-world.
 *       Later sales will use SplitPayment contracts to route funds, with examples in the tests.
 *
 * Unsold tokens (of a successful sale) are redistributed proportionally to investors via Airdrop, as described in:
 * https://medium.com/FundFantasy/airdropping-vs-burning-part-1-613a9c6ebf1c
 *
 * If a sale's soft-cap is not reached, investors will be refunded Ether and the seller refunded tokens.
 *
 * For stable token sales (soft and hard-cap in USD instead of Wei), we rely on MakerDAO's on-chain ETH/USD conversion rate
 * https://developer.makerdao.com/feeds/
 * This approach to mitigating Ether volatility seems to best when analyzing trade-offs, short of selling directly in FIAT.
 */
contract TokenSale is RefundableCrowdsale, PostDeliveryCrowdsale {
  using SafeMath for uint256;
  using SafeERC20 for ERC20;

  address public constant MEDIANIZER_PROXY_ADDRESS = 0xe8f99AEa488aB288f4D787f7FD75b158A5685F8A; // 0xec8be1a5630364292e56d01129e8ee8a9578d7d8;
  address public constant WHITELIST_PROXY_ADDRESS = 0x7223b032180CDb06Be7a3D634B1E10032111F367;  // 0x85a84691547b7ccf19d7c31977a7f8c0af1fb25a;

  MedianizerProxyInterface private medianizerProxy = MedianizerProxyInterface(MEDIANIZER_PROXY_ADDRESS);
  WhitelistProxyInterface private whitelistProxy = WhitelistProxyInterface(WHITELIST_PROXY_ADDRESS);

  uint256 public constant MINIMUM_INVESTMENT_WEI = 1e18;
  uint256 public constant CAP_INVESTMENT_WEI = 1e24;

  address public beneficiary;
  uint256 public cap;
  uint256 public totalTokens;
  uint256 public totalTokensSold = 0;
  bool public usd;
  uint256 public usdPerEthOnFinalize;

  Payments public payments = new Payments();

  mapping(address => uint256) public usdInvestment;

  constructor(
    uint256 _openingTime,
    uint256 _closingTime,
    uint256 _rate,
    address _wallet,
    uint256 _cap,
    ERC20 _token,
    uint256 _goal,
    bool _usd  // if true, both `goal` and `cap` are in units of USD. if false, in ETH
  )
    public
    Crowdsale(_rate, address(payments), _token)
    TimedCrowdsale(_openingTime, _closingTime)
    RefundableCrowdsale(_goal)
    PostDeliveryCrowdsale()
  {
    require(_cap > 0, "cap is not > 0");
    require(_goal < _cap, "goal is not < cap");
    require(_wallet != address(0));
    beneficiary = _wallet;
    cap = _cap;
    usd = _usd;
  }

  function capReached() public view returns (bool) {
    return _reached(cap);
  }

  function goalReached() public view returns (bool) {
    return _reached(goal);
  }

  function hasClosed() public view returns (bool) {
    return _reached(cap.mul(99).div(100)) || super.hasClosed();
  }

  function reverse(address _account) public onlyOwner {
    require(!isFinalized);
    WhitelistInterface whitelist = whitelistProxy.whitelist();
    require(!whitelist.hasRole(_account, "authorized"));

    uint256 balance = balances[_account];
    balances[_account] = 0;
    totalTokensSold = totalTokensSold.sub(balance);
    usdInvestment[_account] = 0;
    weiRaised = weiRaised.sub(balance / rate);
    payments.addPayment(_account, balance / rate);
  }

  function withdrawTokens() public {  // airdrop remaining tokens to investors proportionally
    require(isFinalized && goalReached(), "withdrawTokens requires the TokenSale to be successfully finalized");
    uint256 extra = totalTokens.sub(totalTokensSold).mul(balances[msg.sender]) / totalTokensSold;
    balances[msg.sender] = balances[msg.sender].add(extra);
    super.withdrawTokens();
  }

  function finalization() internal {  // ether refunds enabled for investors, refund tokens to seller
    super.finalization();

    totalTokens = token.balanceOf(address(this));

    MedianizerInterface medianizer = medianizerProxy.medianizer();
    usdPerEthOnFinalize = uint256(medianizer.read());

    if (goalReached()) {
      payments.addPayment(beneficiary, weiRaised);
    } else {
      token.safeTransfer(owner, totalTokens);
    }
  }

  function _getUsdAmount(uint256 _weiAmount) internal view returns (uint256) {
    MedianizerInterface medianizer = medianizerProxy.medianizer();
    uint256 usdPerEth = isFinalized ? usdPerEthOnFinalize : uint256(medianizer.read());
    return _weiAmount.mul(usdPerEth).div(1e18).div(1e18);
  }

  function _preValidatePurchase(address _beneficiary, uint256 _weiAmount) internal {
    require(_weiAmount >= MINIMUM_INVESTMENT_WEI && (balances[_beneficiary] / rate).add(_weiAmount) <= CAP_INVESTMENT_WEI, "_preValidatePurchase invalid _weiAmount");
    super._preValidatePurchase(_beneficiary, _weiAmount);

    WhitelistInterface whitelist = whitelistProxy.whitelist();
    usdInvestment[_beneficiary] = usdInvestment[_beneficiary].add(_getUsdAmount(_weiAmount));
    if (!whitelist.hasRole(_beneficiary, "uncapped")) {
      require(usdInvestment[_beneficiary] <= 100000);
      whitelist.checkRole(_beneficiary, "authorized");
    }

    if (usd) {
      require(_getUsdAmount(weiRaised.add(_weiAmount)) <= cap, "usd raised must not exceed cap");
    } else {
      require(weiRaised.add(_weiAmount) <= cap, "wei raised must not exceed cap");
    }
  }

  function _processPurchase(address _beneficiary, uint256 _tokenAmount) internal {
    totalTokensSold = totalTokensSold.add(_tokenAmount);
    require(totalTokensSold <= token.balanceOf(address(this)), "totalTokensSold raised must not exceed balanceOf `this`");

    super._processPurchase(_beneficiary, _tokenAmount);
  }

  function _reached(uint256 _target) internal view returns (bool) {
    if (usd) {
      return _getUsdAmount(weiRaised) >= _target;
    } else {
      return weiRaised >= _target;
    }
  }
}
