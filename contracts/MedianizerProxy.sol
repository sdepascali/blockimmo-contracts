pragma solidity 0.4.25;

import "openzeppelin-solidity/contracts/ownership/Claimable.sol";


/**
 * @title MedianizerProxy
 * @dev Points to `Medianizer`, enabling it to be upgraded if absolutely necessary.
 *
 * `TokenSale` references `this.medianizer` to locate `Medianizer`.
 * This contract is never intended to be upgraded.
 */
contract MedianizerProxy is Claimable {
  address public medianizer;

  event Set(address medianizer);

  function set(address _medianizer) public onlyOwner {
    medianizer = _medianizer;
    emit Set(medianizer);
  }
}
