pragma solidity 0.4.25;

import "openzeppelin-solidity/contracts/ownership/Ownable.sol";

import "./DividendDistributingToken.sol";


contract LandRegistryInterface {
  function getProperty(string _eGrid) public view returns (address property);
}


contract LandRegistryProxyInterface {
  function owner() public view returns (address);
  function landRegistry() public view returns (LandRegistryInterface);
}


contract WhitelistInterface {
  function checkRole(address _operator, string _permission) public view;
}


contract WhitelistProxyInterface {
  function whitelist() public view returns (WhitelistInterface);
}


/**
 * @title TokenizedProperty
 * @dev An asset-backed security token (a property as identified by its E-GRID (a UUID) in the (Swiss) land registry).
 *
 * Ownership of `this` must be transferred to `ShareholderDAO` before blockimmo will verify `this` as legitimate in `LandRegistry`.
 * Until verified legitimate, transferring tokens is not possible (locked).
 *
 * Tokens can be freely listed on exchanges (especially decentralized / 0x).
 *
 * `this.owner` can make two suggestions that blockimmo will always (try) to take: `setManagementCompany` and `untokenize`.
 * `this.owner` can also transfer or rescind ownership.
 * See `ShareholderDAO` documentation for more information...
 *
 * Our legal framework requires a `TokenizedProperty` must be possible to `untokenize`.
 * Un-tokenizing is also the first step to upgrading or an outright sale of `this`.
 *
 * For both:
 *   1. `owner` emits an `UntokenizeRequest`
 *   2. blockimmo removes `this` from the `LandRegistry`
 *
 * Upgrading:
 *   3. blockimmo migrates `this` to the new `TokenizedProperty` (ie perfectly preserving `this.balances`)
 *   4. blockimmo attaches `owner` to the property (1)
 *   5. blockimmo adds the property to `LandRegistry`
 *
 * Outright sale:
 *   3. blockimmo deploys a new `TokenizedProperty` and adds it to the `LandRegistry`
 *   4. blockimmo configures and deploys a `TokenSale` for the property with `TokenSale.wallet == address(this)`
 *      (raised Ether distributed to current token holders as a dividend payout)
 *        - if the sale is unsuccessful, the new property is removed from the `LandRegistry`, and `this` is added back
 */
contract TokenizedProperty is Ownable, DividendDistributingToken {
  address public constant LAND_REGISTRY_PROXY_ADDRESS = 0xe72AD2A335AE18e6C7cdb6dAEB64b0330883CD56;  // 0x0f5ea0a652e851678ebf77b69484bfcd31f9459b;
  address public constant WHITELIST_PROXY_ADDRESS = 0x7223b032180CDb06Be7a3D634B1E10032111F367;  // 0x85a84691547b7ccf19d7c31977a7f8c0af1fb25a;

  LandRegistryProxyInterface public registryProxy = LandRegistryProxyInterface(LAND_REGISTRY_PROXY_ADDRESS);
  WhitelistProxyInterface public whitelistProxy = WhitelistProxyInterface(WHITELIST_PROXY_ADDRESS);

  uint8 public constant decimals = 18;
  uint256 public constant NUM_TOKENS = 1000000;
  string public symbol;

  string public managementCompany;
  string public name;

  mapping(address => uint256) public lastTransferBlock;
  mapping(address => uint256) public minTransferAccepted;

  event MinTransferSet(address indexed account, uint256 minTransfer);
  event ManagementCompanySet(string managementCompany);
  event UntokenizeRequest();
  event Generic(string generic);

  modifier isValid() {
    LandRegistryInterface registry = LandRegistryInterface(registryProxy.landRegistry());
    require(registry.getProperty(name) == address(this), "invalid TokenizedProperty");
    _;
  }

  constructor(string _eGrid, string _grundstuck) public {
    require(bytes(_eGrid).length > 0, "eGrid must be non-empty string");
    require(bytes(_grundstuck).length > 0, "grundstuck must be non-empty string");
    name = _eGrid;
    symbol = _grundstuck;

    totalSupply_ = NUM_TOKENS * (uint256(10) ** decimals);
    balances[msg.sender] = totalSupply_;
    emit Transfer(address(0), msg.sender, totalSupply_);
  }

  function () public payable {  // dividends
    uint256 value = msg.value;
    require(value > 0, "must send wei in fallback");

    address blockimmo = registryProxy.owner();
    if (blockimmo != address(0)) {  // 1% blockimmo fee
      uint256 fee = value / 100;
      blockimmo.transfer(fee);
      value = value.sub(fee);
    }

    deposit(value);
  }

  function setManagementCompany(string _managementCompany) public onlyOwner isValid {
    managementCompany = _managementCompany;
    emit ManagementCompanySet(managementCompany);
  }

  function untokenize() public onlyOwner isValid {
    emit UntokenizeRequest();
  }

  function emitGenericProposal(string _generic) public onlyOwner isValid {
    emit Generic(_generic);
  }

  function transfer(address _to, uint256 _value) public isValid returns (bool) {
    require(_value >= minTransferAccepted[_to], "tokens transferred less than _to's minimum accepted transfer");
    transferBookKeeping(msg.sender, _to);
    return super.transfer(_to, _value);
  }

  function transferFrom(address _from, address _to, uint256 _value) public isValid returns (bool) {
    require(_value >= minTransferAccepted[_to], "tokens transferred less than _to's minimum accepted transfer");
    transferBookKeeping(_from, _to);
    return super.transferFrom(_from, _to, _value);
  }

  function setMinTransfer(uint256 _amount) public {
    minTransferAccepted[msg.sender] = _amount;
    emit MinTransferSet(msg.sender, _amount);
  }

  function transferBookKeeping(address _from, address _to) internal {
    whitelistProxy.whitelist().checkRole(_to, "authorized");

    creditAccount(_from);  // required for dividends...
    creditAccount(_to);

    lastTransferBlock[_from] = block.number;  //required for voting
    lastTransferBlock[_to] = block.number;
  }
}
